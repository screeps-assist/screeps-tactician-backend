package model

import kotlinx.serialization.Serializable
import kotlinx.serialization.json.JSON

object CommandResultSerialization {
    fun deserializeCommandResultList(jsonString: String): List<CommandResult> = JSON.parse<CommandResultList>(jsonString).items
    //TODO Check serialization of enum
    fun serializeCommandResultList(items: List<CommandResult>): String = JSON.stringify(CommandResultList(items))
}


@Serializable
data class CommandResultList(val items: List<CommandResult> = ArrayList())

@Serializable
data class CommandResult(val id: String, val status: CommandStatus)

@Serializable
enum class CommandStatus {
    PENDING, WAITING, DONE, FAILED
}