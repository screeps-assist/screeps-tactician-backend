package model

import kotlinx.serialization.Optional
import kotlinx.serialization.Serializable
import kotlinx.serialization.json.JSON

object CommandDataSerialization {
    init {
        SpawningCommandData.registerSerializer()
    }
    fun deserializeCommandData(jsonString: String): List<CommandData> {
        return JSON.parse(PolymorphicArrayListSerializer, jsonString) as List<CommandData>
    }

    fun serializeCommandDataList(list: List<CommandData>): String {
        return JSON.stringify(PolymorphicArrayListSerializer, list)
    }
}

@Serializable
sealed class CommandData {
    abstract fun commandId(): String
    abstract fun type() : CommandType
}

@Serializable
enum class CommandType {
    SPAWN
}

@Serializable
data class SpawningCommandData(val id: String,
                               val spawnId: String,
                               val bodyType: BodyType,
                               @Optional val tier: Int = 1): CommandData() {
    override fun type() = CommandType.SPAWN
    override fun commandId() = id

    companion object
    {
        fun registerSerializer() {
            println("registering serialization for ${CommandType.SPAWN.name} object" )
            PolymorphicSerializer.registerSerializer(SpawningCommandData::class, CommandType.SPAWN.name)
        }
    }
}

@Serializable
enum class BodyType {
    BASIC_WORKER, MINER, MINER_BIG, HAULER, SCOUT, CLAIMER
}



