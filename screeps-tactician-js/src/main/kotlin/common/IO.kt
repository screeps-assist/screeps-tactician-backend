package common

/**
 * Represent a potential change which can be applied.
 * Used to address side-effects in a functional programming style similar to IOMonad in Haskell
 */
interface IO<out T> {
    fun isEmpty(): Boolean
    fun applyIO(modifiable: @UnsafeVariance T)
}

/**
 * Wrap a function which may lead to side effects when called
 */
data class IOFunction<T>(private val modification: (T) -> Unit): IO<T> {
    override fun isEmpty() = false
    override fun applyIO(modifiable: T): Unit = modification.invoke(modifiable)
}

/**
 * Represent a no-changes IO operation
**/
internal object EmptyIO: IO<Nothing> {
    override fun applyIO(modifiable: Nothing) {
        //Do nothing
    }
    override fun isEmpty(): Boolean = true
    override fun hashCode(): Int = 1
}

fun <T> emptyIO(): IO<T> = EmptyIO

