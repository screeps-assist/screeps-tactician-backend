package tactician

import model.CommandResult
import model.CommandStatus
import model.SpawningCommandData
import tactician.spawn.Body
import types.base.get
import types.base.global.ERR_BUSY
import types.base.global.ERR_NOT_ENOUGH_ENERGY
import types.base.global.Game
import types.base.global.OK
import types.base.prototypes.structures.StructureSpawn


object SpawningHandler {

    fun handle(data: SpawningCommandData): CommandResult {
        val spawn: StructureSpawn? = Game.spawns[data.spawnId]
        val (cost, parts) = Body(data.tier, data.bodyType).costAndParts()
        return when {
            spawn == null -> CommandResult(data.id, CommandStatus.FAILED)
            spawn.spawning != null -> CommandResult(data.id, CommandStatus.WAITING)
            cost > spawn.energyCapacity -> CommandResult(data.id, CommandStatus.FAILED)
            cost > spawn.energy -> CommandResult(data.id, CommandStatus.WAITING)
            else -> {
                val newName = "${data.bodyType.name}_T${data.tier}_${Game.time}"
                when(spawn.spawnCreep(parts.toTypedArray(), newName)) {
                    OK -> CommandResult(data.id, CommandStatus.DONE)
                    ERR_NOT_ENOUGH_ENERGY, ERR_BUSY -> CommandResult(data.id, CommandStatus.WAITING)
                    else -> CommandResult(data.id, CommandStatus.FAILED)
                }
            }

        }
    }

}