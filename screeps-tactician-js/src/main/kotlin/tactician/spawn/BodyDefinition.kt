package tactician.spawn

import model.BodyType
import model.BodyType.*
import types.base.get
import types.base.global.*


private fun BodyType.template(): Array<BodyPartConstant> {
    return when (this) {
        BASIC_WORKER -> arrayOf(WORK, CARRY, MOVE)
        MINER -> arrayOf(WORK, WORK, MOVE)
        MINER_BIG -> arrayOf(WORK, WORK, WORK, WORK, WORK, MOVE, MOVE)
        HAULER -> arrayOf(CARRY, CARRY, MOVE)
        SCOUT -> arrayOf(MOVE)
        CLAIMER -> arrayOf(CLAIM, MOVE)
    }
}

data class Body(val tier: Int, val bodyType: BodyType) {

    fun costAndParts(): Pair<Int, List<BodyPartConstant>> {
        val template = bodyType.template()
        var cost = 0
        var parts= mutableListOf<BodyPartConstant>()
                for(i in 0 .. tier) {
                    cost += template.sumBy { BODYPART_COST[it] }
                    parts.addAll(template)
                }
        return cost to parts
    }
}