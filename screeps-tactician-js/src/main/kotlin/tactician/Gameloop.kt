package tactician

import types.base.global.Game
import types.base.global.Memory

object Gameloop {

    private var commandOverview : CommandOverview = CommandOverview()

    fun execute() {
        commandOverview = commandOverview.updateFromMemory(Memory).processCommands()
        commandOverview.resultsToMemory(Game).applyIO(Memory)
    }
}

