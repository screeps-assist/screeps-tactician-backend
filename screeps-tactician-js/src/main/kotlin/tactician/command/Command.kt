package tactician.command

import model.CommandData
import model.CommandResult
import model.SpawningCommandData
import tactician.Context
import tactician.SpawningHandler


class Command(val data: CommandData) {

    val id = data.commandId()

    fun execute(context: Context): CommandResult {
        return when(data) {
            is SpawningCommandData -> SpawningHandler.handle(data)
        }
    }
}
