package tactician

import common.IO
import common.IOFunction
import common.emptyIO
import model.*
import model.CommandDataSerialization.deserializeCommandData
import model.CommandResultSerialization.deserializeCommandResultList
import model.CommandResultSerialization.serializeCommandResultList
import tactician.command.Command
import tactician.command.ResultsInProgress
import tactician.command.ResultsInProgress.MemoryChanges.*
import types.base.global.Game
import types.base.global.Memory

data class CommandOverview(val lastKnownUpdateTick: Int = 0,
                           val pendingQueue: List<Command> = emptyList(),
                           val resultsInProgress: ResultsInProgress = ResultsInProgress()) {

    fun updateFromMemory(memory: Memory): CommandOverview = when {
        memory.lastQueueCompletedTick > memory.lastUpdateTick -> CommandOverview(lastKnownUpdateTick = memory.lastUpdateTick)
        memory.lastUpdateTick == lastKnownUpdateTick -> copy(resultsInProgress = resultsInProgress.resetQueueStates())
        else -> {
            println("Loading pending queue and results in progress from memory")
            val pendingQueue = memory.commandQueue.map { Command(it)}
            val resultsInProgress: Map<String, CommandResult> = memory.commandResults.associateBy({it.id}, {it})
            println("resultsInProgress size ${resultsInProgress.size}")
            CommandOverview(memory.lastUpdateTick, pendingQueue, ResultsInProgress(commandResultsMap = resultsInProgress))
        }
    }

    fun processCommands(): CommandOverview {
        val newResults = pendingQueue.fold(resultsInProgress) { resultsAccumulator, command ->
            resultsAccumulator.processCommand(command)
        }
        return copy(resultsInProgress = newResults)
    }

    fun resultsToMemory(game: Game): IO<Memory> = when (resultsInProgress.memoryChanges()) {
        NO_CHANGES -> emptyIO()
        QUEUE_MODIFIED_INCOMPLETE -> IOFunction { it ->
            run {
                println("Saving results in progress for toCommand queue")
                it.commandResults = resultsInProgress.commandResultsMap.values.toList()
            }
        }
        QUEUE_MODIFIED_COMPLETED -> IOFunction { it ->
            run {
                println("Updating complete queue tick and saving results")
                it.lastQueueCompletedTick = game.time
                it.commandResults = resultsInProgress.commandResultsMap.values.toList()
            }
        }
    }

    private val Memory.commandQueue: List<CommandData>
        get() {
            return try {
                val internal = this.asDynamic().commandQueue as? String?
                if (internal == null) emptyList() else deserializeCommandData(internal)
            } catch (e: Error) {
                println("Error while reading toCommand queue ${e.message}")
                emptyList()
            }
        }

    private var Memory.lastQueueCompletedTick: Int
        get() {
            return this.asDynamic().commandQueueStatus?.lastQueueCompletedTick as? Int ?: 0
        }
        set(value) {
            this.asDynamic().commandQueueStatus.lastQueueCompletedTick = value
        }

    private val Memory.lastUpdateTick: Int
        get() {
            return this.asDynamic().commandQueueStatus?.lastUpdateTick as? Int ?: 0
        }

    private var Memory.commandResults: List<CommandResult>
        get() {
            return try {
                val internal = this.asDynamic().commandQueueStatus?.results as? String?
                if (internal == null) emptyList() else deserializeCommandResultList(internal)
            } catch (e: Error) {
                println("Error while reading toCommand result ${e.message}")
                emptyList()
            }
        }
        set(value) {
            this.asDynamic().commandQueueStatus.results = serializeCommandResultList(value)
        }
}
