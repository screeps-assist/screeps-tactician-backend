package tactician.command

import tactician.Context
import tactician.command.ResultsInProgress.MemoryChanges.*
import model.CommandResult
import model.CommandStatus


data class ResultsInProgress(val queueModified: Boolean? = null, val queueCompleted: Boolean? = null, val commandResultsMap: Map<String, CommandResult> = emptyMap()) {

    enum class MemoryChanges { NO_CHANGES, QUEUE_MODIFIED_COMPLETED, QUEUE_MODIFIED_INCOMPLETE }

    fun memoryChanges(): MemoryChanges = when {
        queueModified == null || queueCompleted == null -> NO_CHANGES
        queueModified && queueCompleted -> QUEUE_MODIFIED_COMPLETED
        queueModified && !queueCompleted -> QUEUE_MODIFIED_INCOMPLETE
        else -> MemoryChanges.NO_CHANGES
    }

    fun resetQueueStates(): ResultsInProgress = copy(commandResultsMap = commandResultsMap)

    fun processCommand(command: Command): ResultsInProgress {
        val previousResult = commandResultsMap[command.id]?: CommandResult(command.id, CommandStatus.PENDING)
        val newResult = if(previousResult.status.commandCompleted) previousResult else command.execute(Context)
        val newQueueModified = queueModified?: false || newResult != previousResult
        val newQueueCompleted = queueCompleted?: true && newResult.status.commandCompleted
        return copy(queueModified = newQueueModified, queueCompleted = newQueueCompleted,
                commandResultsMap = commandResultsMap.plus(Pair(command.id, newResult)))
    }

    private val CommandStatus.commandCompleted: Boolean
        get() {
            return when(this) {
                CommandStatus.DONE, CommandStatus.FAILED -> true
                CommandStatus.PENDING, CommandStatus.WAITING -> false
            }
        }
}
